import Vue from 'vue/dist/vue.esm';
import MyComponent from './app.vue';
import Sample from './components/Sample.vue';
import store from './store';

new Vue({
  el: '#hello',
  components: {
    'my-component': MyComponent,
    'sample': Sample
  },
  store
})